$( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
    $( "#draggable5" ).draggable({
        snap: '#droppable',
    snapTolerance: 50,
    cursor: 'move',
    opacity : 0.3
    });
    $( "#resizable5" ).resizable({
       
    });
  } );