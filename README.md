## Team Name - Techno Geeks

### Project Overview
----------------------------------

* Dynamic Perosnalize UI Creation - 
* A personalized UI where user can drag/drop any of these components and build our      his/her own UI

### Solution Description
----------------------------------
* Adding widgets to the web app
* Creating a workspace 
* Making the widgets draggable and droppable in the workspace

#### Architecture Diagram

Affix an image of the flow diagram/architecture diagram of the solution

#### Technical Description

Technical tools used:
* Java script, node js
* Jquery UI

### Team Members
----------------------------------

Amit Kumar Panda, Email ID - b217003@iiit-bh.ac.in
Ankita Patra, Email ID - b217027@iiit-bh.ac.in
Nitish Kumar Dash, Email ID- b217050@iiit-bh.ac.in
Sameer Patnaik, Email ID - b217035@iiit-bh.ac.in
